#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INITIAL_LENGTH 10

long search(long *array, long value, int len) {
    for(int i = 0; i < len; i++) {
        if(array[i] == value) {
            printf("value found: %ld\n", value);
            return value;
        }
    }
    return 0;
}

void printArray(long *array, int len) {
    for(int i = 0; i < len; i++) {
        printf("%ld\n", array[i]);
    }
}
 
int main(int arg, char *argv[]) {
    FILE *input;
    long total = 0;
    long *duplicates;
    long value;
    int i=0;
    long result = 0;

    input = fopen("input.txt", "r");
    int len = INITIAL_LENGTH;

    duplicates = malloc(sizeof(long) * len);

    if(duplicates == NULL) {
        perror("Failed to allocate memory");
        abort();
    }

    printArray(duplicates, len);

    while((fscanf(input, "%ld", &value) ) != EOF) {
        total += (long) value;
        if(i == len) {
            len = len * 2;
            duplicates = realloc(duplicates, sizeof(long) * len);
            if(duplicates == NULL) {
                perror("Failed to reallocate memory");
                abort();
            }
        }
        result = search(duplicates, total, i);
        duplicates[i] = total;
        i++;
    }        

    printArray(duplicates, i);

    fclose(input);
    free(duplicates);

    printf("result: %ld", result);
}
